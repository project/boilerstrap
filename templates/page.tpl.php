<header>
  <div class="container">
  <?php print render($page['header']); ?>
  </div>
</header>
<nav id="main-menu">
  <div class="container">
    <div class="<?php print $navbar_class; ?>">
      <div class="navbar-inner">
        <div class="container">
          <div class="brand"><?php print l( $site_name, '<front>' ); ?></div>
          <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('nav')))); ?>  
          <?php if( isset($search_box) ): print $search_box; endif; ?>
          <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('class' => array('nav', 'pull-right')))); ?>
        </div>
      </div>
    </div>
  </div>
</nav>

<div id="main" class="container">
  <div class="row">
    <div id="content" class="span<?php print $page['sidebar'] ? 9 : 12; ?>">
      <?php if ($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
      <?php endif; ?>
      <?php print $messages; ?>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php if ($tabs): ?><?php print render($tabs); ?><?php endif; ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>
   
    <?php if ($page['sidebar']): ?>
    <div id="sidebar" class="span3">
      <?php print render($page['sidebar']); ?>
    </div>
    <?php endif; ?>
  </div>
</div>

<footer>
  <div class="container">
  <?php print render($page['footer']); ?>
  </div>
</footer>