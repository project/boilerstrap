(function ($) {  
  $(document).ready( function(){
    $('.alert .close').click( function(){
      $(this).closest('.alert').fadeOut();
    } );
  });
})(jQuery);