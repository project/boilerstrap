<?php

/**
 * Implements hook_html_head_alter().
 */
function boilerstrap_html_head_alter(&$head_elements) {
  unset( $head_elements['system_meta_content_type'] );
  $meta = array(
    'charset' => array('charset' => "utf-8"),
    'content_type' => array('http-equiv' => "X-UA-Compatible", 'content' => "IE=edge,chrome=1"),
    'viewport' => array('name' => "viewport", 'content' => "width=device-width, initial-scale=1.0"),
  );
  $count = count( $meta );
  foreach ( $meta as $tag => $attributes ) {
    $head_elements['boilerstrap_' . $tag] = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => $attributes,
      '#weight' => -1000 - $count
    );
    $count--;
  }
}

function boilerstrap_preprocess_html(&$variables) {  
  if( theme_get_setting('navbar_fixed') ) {
    $variables['classes_array'][] = 'navbar-position-fixed';
  }
  else {
    $variables['classes_array'][] = 'navbar-position-relative';
  }
}

function boilerstrap_preprocess_page(&$variables) {
  if ( theme_get_setting('navbar_search') && module_exists('search')) {
    $search_form = drupal_get_form('search_form');
    $search_form['#attributes']['class'][] = 'navbar-search';
    unset( $search_form ['basic']['keys']['#title'] );
    $search_form ['basic']['keys']['#attributes']['class'] = array('search-query');
    $search_form ['basic']['keys']['#attributes']['placeholder'] = 'Search'; 
    $search_form ['basic']['submit']['#attributes']['class'] = array('hidden');
    $variables['search_box'] = drupal_render( $search_form );
  }
  
  $variables['navbar_class'] = array('navbar');
  if( theme_get_setting('navbar_fixed') ) {
    $variables['navbar_class'][] = 'navbar-fixed-top';
  }
  $variables['navbar_class'] = implode(' ', $variables['navbar_class']);
  
}

function boilerstrap_status_messages($variables) {
  
  $display = $variables['display'];
  $output = '';
  
  $status_heading = array(
    'status' => t('Status message'), 
    'error' => t('Error message'), 
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    
    if ( $type == 'status' ) {
      $type = 'info';
    }
  
    $output .= "<div class=\"alert alert-$type\">";  
    $output .= "<a class='close'>&times;</a>";
    
    if (!empty($status_heading[$type])) {
      $output .= '<h3 class="alert-heading hidden">' . $status_heading[$type] . "</h3>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

function boilerstrap_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  $divider = decode_entities( theme_get_setting('breadcrumb_divider') );
  $count = count( $breadcrumb );
  if ( $count > 0 ) {
    if ( $count > 1 ) {
      foreach ( $breadcrumb as $key => &$crumb ) {
        if ( $key < ( $count - 1 )  ) {
          $crumb .= '<span class="divider">'.$divider.'</span>';
        }
      }
    }
    $output = '<h2 class="hidden">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode($breadcrumb) . '</div>';
    return $output;
  }
}

function boilerstrap_menu_local_tasks(&$variables) {
  $output = '';
  $style = theme_get_setting('tabs_style');
  $stacked = theme_get_setting('tabs_stacked');
  $classes = array('nav', $style);
  if( $stacked ) {
    $classes[] = 'nav-stacked';
  }
  foreach ( array('primary', 'secondary') as $menu ) {
    if ( !empty( $variables[$menu] ) ) {   
      $variables[$menu]['#prefix'] = '<h2 class="hidden">' . t('@menu tabs', array('@menu' => ucwords( $menu ) )) . '</h2>';
      $variables[$menu]['#prefix'] .= '<ul class="' . implode(' ', $classes) . ' ' . $menu.'-tabs">';
      $variables[$menu]['#suffix'] = '</ul>';
      $output .= drupal_render($variables[$menu]);
    }
  }
  return $output;
}

function boilerstrap_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
 
  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'disabled';
    $element['#attributes']['disabled'][] = 'disabled';
  }
  $element['#attributes']['class'][] = 'btn';
  
  return '<button' . drupal_attributes($element['#attributes']) . '>'.$element['#value'].'</button>';
}

function boilerstrap_table($variables) {
  $header = $variables['header'];
  $rows = $variables['rows'];
  $table_attributes = $variables['attributes'];
  $caption = $variables['caption'];
  $colgroups = $variables['colgroups'];
  $sticky = $variables['sticky'];
  $empty = $variables['empty'];  
  // Add sticky headers, if applicable.
  if (count($header) && $sticky) {
    drupal_add_js('misc/tableheader.js');
    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $attributes['class'][] = 'sticky-enabled';
  }
  $striping = FALSE;
  $table_attributes['class'][] = 'table';
   
  $output = '';

  if (isset($caption)) {
    $output .= '<caption>' . $caption . "</caption>\n";
  }

  // Format the table columns:
  if (count($colgroups)) {
    foreach ($colgroups as $number => $colgroup) {
      $attributes = array();

      // Check if we're dealing with a simple or complex column
      if (isset($colgroup['data'])) {
        foreach ($colgroup as $key => $value) {
          if ($key == 'data') {
            $cols = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cols = $colgroup;
      }

      // Build colgroup
      if (is_array($cols) && count($cols)) {
        $output .= ' <colgroup' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cols as $col) {
          $output .= ' <col' . drupal_attributes($col) . ' />';
        }
        $output .= " </colgroup>\n";
      }
      else {
        $output .= ' <colgroup' . drupal_attributes($attributes) . " />\n";
      }
    }
  }

  // Add the 'empty' row message if available.
  if (!count($rows) && $empty) {
    $header_count = 0;
    foreach ($header as $header_cell) {
      if (is_array($header_cell)) {
        $header_count += isset($header_cell['colspan']) ? $header_cell['colspan'] : 1;
      }
      else {
        $header_count++;
      }
    }
    $rows[] = array(array(
        'data' => $empty,
        'colspan' => $header_count,
        'class' => array('empty', 'message'),
      ));
  }

  // Format the table header:
  if (count($header)) {
    $ts = tablesort_init($header);
    // HTML requires that the thead tag has tr tags in it followed by tbody
    // tags. Using ternary operator to check and see if we have any rows.
    $output .= (count($rows) ? ' <thead><tr>' : ' <tr>');
    foreach ($header as $cell) {
      $cell = tablesort_header($cell, $header, $ts);
      $output .= _theme_table_cell($cell, TRUE);
    }
    // Using ternary operator to close the tags based on whether or not there are rows
    $output .= (count($rows) ? " </tr></thead>\n" : "</tr>\n");
  }
  else {
    $ts = array();
  }

  // Format the table rows:
  if (count($rows)) {
    $output .= "<tbody>\n";
    $flip = array(
      'even' => 'odd',
      'odd' => 'even',
    );
    $class = 'even';
    foreach ($rows as $number => $row) {
      $attributes = array();

      // Check if we're dealing with a simple or complex row
      if (isset($row['data'])) {
        foreach ($row as $key => $value) {
          if ($key == 'data') {
            $cells = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cells = $row;
      }
      if (count($cells)) {
        
        
        // Add odd/even class
        if (empty($row['no_striping'])) {
          $striping = TRUE;
        }
        
        // Build row
        $output .= ' <tr' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cells as $cell) {
          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= _theme_table_cell($cell);
        }
        $output .= " </tr>\n";
      }
    }
    $output .= "</tbody>\n";
  }

  $output .= "</table>\n";
  
  if ( $striping ) {
    $table_attributes['class'][] = 'table-striped';
  }  

//  $table_attributes['class'][] = 'table-bordered';
  
  $output = '<table' . drupal_attributes($table_attributes) . ">" . $output;
  
  return $output;
}

function boilerstrap_pager($variables) {
  $tags = $variables['tags'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $quantity = $variables['quantity'];
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
  $li_previous = theme('pager_previous', array('text' => (isset($tags[1]) ? $tags[1] : t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_next = theme('pager_next', array('text' => (isset($tags[3]) ? $tags[3] : t('next ›')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
  $li_last = theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => array('pager-first'), 
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => array('pager-previous'), 
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => array('pager-ellipsis'), 
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => array('pager-item'), 
            'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => array('pager-current'), 
            'data' => l( $i , $_GET['q'] ),
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => array('pager-item'), 
            'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => array('pager-ellipsis'), 
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => array('pager-next'), 
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => array('pager-last'), 
        'data' => $li_last,
      );
    }
    $pager = '<h2 class="invisible">' . t('Pages') . '</h2><div class="pagination">' .theme('item_list', array(
      'items' => $items, 
     // 'attributes' => array('class' => array('pagination')),
    )) . '</div>';
        
    return $pager;
  }
}