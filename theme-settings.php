<?php

function boilerstrap_form_system_theme_settings_alter(&$form, &$form_state) {
  
  $form['theme_settings']['breadcrumb_divider'] = array(
    '#type' => 'textfield',
    '#title' => t('Breadcrumb Divider'),
    '#default_value' => theme_get_setting('breadcrumb_divider'),
    '#attributes' => array('class' => array('span1') )
  );
  
  $form['theme_settings']['tabs_style'] = array(
    '#type' => 'radios',
    '#title' => t('Tabs Style'),
    '#default_value' => theme_get_setting('tabs_style'),
    '#options' => array('nav-tabs' => 'Tabs', 'nav-pills' => 'Pills')
  );
  
  $form['theme_settings']['tabs_stacked'] = array(
    '#type' => 'checkbox',
    '#title' => t('Stacked'),
    '#default_value' => theme_get_setting('tabs_stacked'),
  );
  
  $form['theme_settings']['navbar_search'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show search in navbar'),
    '#default_value' => theme_get_setting('navbar_search'),
  );
  
  $form['theme_settings']['navbar_fixed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fixed navbar'),
    '#default_value' => theme_get_setting('navbar_fixed'),
    '#description' => t('If the toolbar module is enabled, the toolbar and shortcut bar will be placed on the bottom of the page')
  );
  
}
?>